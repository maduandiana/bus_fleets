import 'package:flutter/material.dart';
import './Bus.dart';
import './passengers.dart';
import './schedule.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Расписание',
      home: SchedulePage(),
    );
  }
}

import 'package:flutter/material.dart';
import 'Bus.dart';
import 'pdetails.dart';
import './schedule.dart';

class PassengersPage extends StatelessWidget {
  final String title;

  PassengersPage({Key key, this.title}) : super(key: key);
  List<Bus> buses = [
    Bus(
        busName: 'YUTONG',
        busNumber: 'KZ 888 KN 02',
        seats: "32",
        busImage: "assets/bus1.png",
        depTime: "18:39",
        arrTime: "06:10",
        depDate: "06.06.2020 Thu",
        arrDate: "07.06.2020 Thu",
        direction: 'Тараз-Алматы',
        when: 'Сегодня'),
    Bus(
        busName: 'Mad test',
        busNumber: 'KZ 666 MD 09',
        seats: "36",
        busImage: "assets/bus2.png",
        depTime: "21:49",
        arrTime: "02:30",
        depDate: "07.06.2020 Thu",
        arrDate: "08.06.2020 Fri",
        direction: 'Шымкент-Алматы',
        when: 'Завтра'),
    Bus(
        busName: 'Bus 001',
        busNumber: 'KZ 000 DM 01',
        seats: "30",
        busImage: "assets/bu3.png",
        depTime: "11:32",
        arrTime: "17:10",
        depDate: "07.06.2020 Thu",
        arrDate: "08.06.2020 Thu",
        direction: 'Тараз-Астана',
        when: ''),
  ];
  String filterText = '';

  Widget busDetailCard(Bus, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PassengersDetails()),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(12.0),
        margin: EdgeInsets.all(10),
        decoration: new BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              spreadRadius: 2,
              blurRadius: 2,
              offset: Offset(0, 2), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              Bus.when,
              style: TextStyle(
                  color: Colors.green,
                  fontSize: 24,
                  fontWeight: FontWeight.w600),
            ),
            Row(
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Container(
                          width: 120.0,
                          height: 150.0,
                          decoration: new BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage(Bus.busImage)))),
                    ),
                    Row(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Bus.busName,
                              style: TextStyle(
                                  color: Colors.grey[900],
                                  fontSize: 22,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              Bus.busNumber,
                              style: TextStyle(
                                  color: Colors.grey[800], fontSize: 16),
                            ),
                            Text(
                              Bus.seats + ' мест',
                              style: TextStyle(
                                  color: Colors.grey[800], fontSize: 16),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      Bus.direction,
                      style: TextStyle(
                        color: Colors.grey[800],
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    Text(
                      'Отправление',
                      style: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          height: 2),
                    ),
                    Text(
                      'Дата - ' + Bus.depDate,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16),
                    ),
                    Text(
                      'Время - ' + Bus.depTime,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16),
                    ),
                    Text(
                      'Прибытие',
                      style: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          height: 2),
                    ),
                    Text(
                      'Дата - ' + Bus.arrDate,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16),
                    ),
                    Text(
                      'Время - ' + Bus.arrTime,
                      style: TextStyle(color: Colors.grey[800], fontSize: 16),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          "Выберите рейс",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
            children: buses.map((p) {
          if (p.busName != filterText) {
            return busDetailCard(p, context);
          }
        }).toList()),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            Container(
              height: 140.0,
              child: DrawerHeader(
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0, color: Color(0xFFFF000000)),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Test Company',
                      style: TextStyle(color: Colors.grey, fontSize: 22),
                    ),
                    Text(
                      "Maduan Diana",
                      style: TextStyle(
                          color: Colors.grey[800],
                          fontSize: 26,
                          fontWeight: FontWeight.w700,
                          height: 3),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Продажа билетов',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.view_list_outlined,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
            ListTile(
              title: Text(
                'Список администраторов',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.contacts_rounded,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
            ListTile(
              title: Text(
                'Автобусы',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.directions_bus,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
            ListTile(
              title: Text(
                'Статистика',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.bar_chart_rounded,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
            ListTile(
              title: Text(
                'Пассажиры',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.airline_seat_recline_normal_rounded,
                size: 35,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PassengersPage()),
                );
              },
            ),
            ListTile(
              title: Text(
                'Расписание',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.calendar_view_day_rounded,
                size: 35,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SchedulePage()),
                );
              },
            ),
            ListTile(
              title: Text(
                'История',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w700),
              ),
              leading: Icon(
                Icons.auto_stories,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
            ListTile(
              title: Text(
                'Настройки',
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 20,
                    fontWeight: FontWeight.w800),
              ),
              leading: Icon(
                Icons.settings_rounded,
                size: 35,
              ),
              onTap: () {
                print("Clicked");
              },
            ),
          ],
        ),
      ),
    );
  }
}

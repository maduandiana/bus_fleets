class Passengers {
  String name;
  String seatNo;
  String seatType;
  String status;
  String phone;
  String cost;
  Passengers(
      {this.name,
      this.seatNo,
      this.seatType,
      this.status,
      this.phone,
      this.cost});
}

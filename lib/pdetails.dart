import 'package:flutter/material.dart';
import './passclass.dart';

class PassengersDetails extends StatefulWidget {
  @override
  _MyListState createState() => _MyListState();
}

class _MyListState extends State<PassengersDetails> {
  List<Passengers> passengers = [
    Passengers(
        name: 'Diana',
        seatNo: '0 A',
        seatType: 'верхний',
        status: 'OFFLINE',
        phone: '87477772345',
        cost: '3000'),
    Passengers(
        name: 'Arslan',
        seatNo: '0 B',
        seatType: 'верхний',
        status: 'OFFLINE',
        phone: '87475571231',
        cost: '3500'),
    Passengers(
        name: 'Assel',
        seatNo: '1',
        seatType: 'нижний',
        status: 'ONLINE',
        phone: '87477172399',
        cost: '4000'),
    Passengers(
        name: 'Timur',
        seatNo: '1',
        seatType: 'верхний',
        status: 'ONLINE',
        phone: '87071110033',
        cost: '3200'),
  ];
  List<Passengers> emptyList = [
    Passengers(
        name: 'Нет имени',
        seatNo: '2 A',
        seatType: 'верхний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3200'),
    Passengers(
        name: 'Нет имени',
        seatNo: '2 B',
        seatType: 'верхний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3200'),
    Passengers(
        name: 'Нет имени',
        seatNo: '2',
        seatType: 'нижний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3500'),
    Passengers(
        name: 'Нет имени',
        seatNo: '2',
        seatType: 'верхний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3500'),
    Passengers(
        name: 'Нет имени',
        seatNo: '3',
        seatType: 'нижний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3200'),
    Passengers(
        name: 'Нет имени',
        seatNo: '3',
        seatType: 'верхний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3000'),
    Passengers(
        name: 'Нет имени',
        seatNo: '4',
        seatType: 'нижний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3200'),
    Passengers(
        name: 'Нет имени',
        seatNo: '4',
        seatType: 'верхний',
        status: 'no type',
        phone: '+7(---) --- -- --',
        cost: '3000'),
  ];
  String filterText = '';
  showAlertDialog(pname, BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("Да", style: TextStyle(color: Colors.green, fontSize: 18)),
      onPressed: () {
        setState(() {
          passengers.removeWhere((element) => element.name == pname);
        });
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("Нет", style: TextStyle(color: Colors.green, fontSize: 18)),
      onPressed: () {
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Вы хотите отменить покупку билета?"),
      actions: [cancelButton, okButton],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget passList(Passengers, BuildContext context) {
    return GestureDetector(
        onTap: () {
          showModalBottomSheet<void>(
            isScrollControlled: true,
            context: context,
            builder: (BuildContext context) {
              return Container(
                height: 700,
                color: Colors.white,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Center(
                        child: Text(
                          'Информация о пассажире',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 22,
                              fontWeight: FontWeight.w500,
                              height: 3),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.4,
                                  offset: Offset(0, 2),
                                )
                              ]),
                          child: TextField(
                            decoration: new InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: BorderSide.none),
                                hintStyle: new TextStyle(color: Colors.black),
                                hintText: Passengers.name,
                                fillColor: Colors.white70),
                          )),
                      Container(
                          margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.4,
                                  offset: Offset(0, 2),
                                )
                              ]),
                          child: TextField(
                            decoration: new InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: BorderSide.none),
                                hintStyle: new TextStyle(color: Colors.black),
                                hintText: Passengers.phone,
                                fillColor: Colors.white70),
                          )),
                      Container(
                          margin: EdgeInsets.only(left: 30, right: 30, top: 20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: new BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 2.0,
                                  spreadRadius: 0.4,
                                  offset: Offset(0, 2),
                                )
                              ]),
                          child: TextField(
                            decoration: new InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(10.0),
                                    borderSide: BorderSide.none),
                                hintStyle: new TextStyle(color: Colors.green),
                                hintText: 'Почта',
                                fillColor: Colors.green[600]),
                          )),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20.0, left: 60, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Место',
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(20),
                                  margin: const EdgeInsets.only(top: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 2.0,
                                          spreadRadius: 0.4,
                                          offset: Offset(0, 2),
                                        )
                                      ]),
                                  child: Text(
                                    Passengers.seatNo +
                                        ' ' +
                                        Passengers.seatType,
                                    style: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        height: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20.0, left: 30, bottom: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  'Цена',
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only(
                                      top: 20, bottom: 20, left: 40, right: 40),
                                  margin: const EdgeInsets.only(top: 20),
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 2.0,
                                          spreadRadius: 0.4,
                                          offset: Offset(0, 2),
                                        )
                                      ]),
                                  child: Text(
                                    Passengers.cost,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        height: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(children: <Widget>[
                        Container(
                            height: 60,
                            margin: const EdgeInsets.only(
                                left: 50, top: 10, bottom: 10, right: 10),
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: new BorderRadius.circular(15.0),
                            ),
                            child: OutlineButton(
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(15.0)),
                              borderSide: BorderSide(color: Colors.green),
                              onPressed: () {},
                              child: Text(
                                "Изменить",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )),
                        Container(
                            height: 60,
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: new BorderRadius.circular(15.0),
                            ),
                            child: OutlineButton(
                              onPressed: () {},
                              shape: new RoundedRectangleBorder(
                                  borderRadius:
                                      new BorderRadius.circular(15.0)),
                              borderSide: BorderSide(color: Colors.green),
                              child: Text(
                                "Отправить чек",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            )),
                      ]),
                      Container(
                          height: 60,
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: OutlineButton(
                            onPressed: () {
                              showAlertDialog(Passengers.name, context);
                            },
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(15.0)),
                            borderSide: BorderSide(color: Colors.green),
                            child: Text(
                              "Отменить покупку билета",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          )),
                    ],
                  ),
                ),
              );
            },
          );
        },
        child: Container(
          padding: const EdgeInsets.all(0),
          margin: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(
                Passengers.name,
                style: TextStyle(
                    color: Colors.grey[700],
                    fontSize: 22,
                    fontWeight: FontWeight.w500),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: Text(
                      Passengers.seatNo,
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 22,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Text(
                    Passengers.seatType,
                    style: TextStyle(color: Colors.grey[800], fontSize: 16),
                  )
                ],
              ),
              Container(
                padding: const EdgeInsets.all(12.0),
                margin: EdgeInsets.all(5),
                decoration: new BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30)),
                ),
                child: Text(
                  '  ' + Passengers.status + '  ',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          "Пассажиры",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      'Имя',
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 22,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      'Место',
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 22,
                          fontWeight: FontWeight.w500),
                    ),
                    Text(
                      'Тип',
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 22,
                          fontWeight: FontWeight.w500),
                    ),
                  ]),
            ),
            Column(
                children: passengers.map((p) {
              return passList(p, context);
            }).toList()),
            Text(
              'Свободные места',
              style: TextStyle(
                  color: Colors.grey[700],
                  fontSize: 22,
                  fontWeight: FontWeight.w500),
            ),
            Column(
                children: emptyList.map((p) {
              return passList(p, context);
            }).toList()),
          ],
        ),
      ),
    );
  }
}

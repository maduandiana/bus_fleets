class Bus {
  String direction;
  String depTime;
  String arrTime;
  String depDate;
  String arrDate;
  String busName;
  String busNumber;
  String seats;
  String busImage;
  String when;
  Bus(
      {this.direction,
      this.depTime,
      this.arrTime,
      this.depDate,
      this.arrDate,
      this.busName,
      this.busNumber,
      this.busImage,
      this.seats,
      this.when});
}
